$(document).ready(function(){
    customGameStats("5177");
    $("#game-table-container").slideUp(200).fadeIn(100);
});

$("#top-inputs").on("submit", function (event) {
    event.preventDefault();
    var answer = $("#text-input").val();
    customGameStats(answer);
    //$("#player-name").text(answer);
    document.getElementById("top-inputs").reset();
    $("#game-table-container").slideUp(200).fadeIn(100);
});


/*
var insertTurn = function(tn, ts, st, pn) {

      var table = document.getElementById("game-table");
      var row = table.insertRow(-1);
      var turn = row.insertCell(0);
      var turnScore = row.insertCell(1);
      var scoreType = row.insertCell(2);
      var playerName = row.insertCell(3);
      turn.innerHTML = tn;
      turnScore.innerHTML = ts;
      scoreType.innerHTML = st;
      playerName.innerHTML = pn;
};


var insertDice = function(dice) {

      var diceTable = document.getElementById("dice-table");
      var row = diceTable.insertRow(-1);
      var die = row.insertCell(0);

            switch (dice) {

              case 1:
                  dice = "dice1";
                  break;
              case 2:
                  dice = "dice2";
                  break;
              case 3:
                  dice = "dice3";
                  break;
              case 4:
                  dice = "dice4";
                  break;
              case 5:
                  dice = "dice5";
                  break;
              case 6:
                  dice = "dice6";
                  break;

            }

      die.innerHTML = "<img src=" + "img/" + dice + ".png>";
};

var insertFiveDice = function(one, two, three, four, five) {

  insertDice(one);
  insertDice(two);
  insertDice(three);
  insertDice(four);
  insertDice(five);

};

insertFiveDice(1, 2, 2, 6, 6);

//insertTurn("1", "24", "Sixes", "Beppe");

*/

var customGameStats = function (gameID)
{

    var xHttp;

    if (gameID === "")
    {
        document.getElementById("game-table-container").innerHTML = "";
        return;
    }
    xHttp = new XMLHttpRequest();
    xHttp.onreadystatechange = function ()
    {
        if (xHttp.readyState === 4 && xHttp.status === 200)
        {
            document.getElementById("game-table-container").innerHTML = xHttp.responseText;
        }
    };
    xHttp.open("GET", "getGameStats.jsp?q=" + gameID, true);
    xHttp.send();
};

