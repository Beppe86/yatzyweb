$(document).ready(function () {
    customHighScore("10");
    $("#high-score-container").slideUp(200).fadeIn(100);
});

$("#top20").click(function () {
    customHighScore("20");
    $("#high-score-container").slideUp(200).fadeIn(100);
});


$("#top100").click(function () {
    customHighScore("100");
    $("#high-score-container").slideUp(500).fadeIn(1000);
});

$("#top-inputs").on("submit", function (event) {
    event.preventDefault();
    var answer = $("#text-input").val();
    customHighScore(answer);
    $("#high-score-container").slideUp(200).fadeIn(100);
});


var insertNewPlayer = function (pn, highScore, gID, gd) {

    var table = document.getElementById("high-score");
    var row = table.insertRow(-1);
    var playerName = row.insertCell(0);
    var hiScore = row.insertCell(1);
    var gameId = row.insertCell(2);
    var gameDate = row.insertCell(3);
    playerName.innerHTML = pn;
    hiScore.innerHTML = highScore;
    gameId.innerHTML = gID;
    gameDate.innerHTML = gd;
};

var customHighScore = function (resultNumber)
{

    var xHttp;

    if (resultNumber === "")
    {
        document.getElementById("high-score-container").innerHTML = "";
        return;
    }
    xHttp = new XMLHttpRequest();
    xHttp.onreadystatechange = function ()
    {
        if (xHttp.readyState === 4 && xHttp.status === 200)
        {
            document.getElementById("high-score-container").innerHTML = xHttp.responseText;
        }
    };
    xHttp.open("GET", "getHighScore.jsp?q=" + resultNumber, true);
    xHttp.send();
};
