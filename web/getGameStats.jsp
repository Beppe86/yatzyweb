<%-- 
    Document   : getGameStats
    Created on : Mar 5, 2016, 10:27:45 AM
    Author     : Kermit
--%>

<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>



<sql:setDataSource var="snapshot" driver="com.microsoft.sqlserver.jdbc.SQLServerDriver"
                   url="jdbc:sqlserver://62.101.44.83:1433;DatabaseName=KERPE"
                   user="KerpeAdmin"  password="kerpe"/>

<sql:query dataSource="${snapshot}" var="result">
    EXECUTE WebGetGameStats <%= request.getParameter("q") %>
</sql:query>


<table id="high-score">
    <tr>
        <th>Turn</th>
        <th>Turn score</th>
        <th>Score type</th>
        <th style="width:100px">Player name</th>
        <th>D1</th>
        <th>D2</th>
        <th>D3</th>
        <th>D4</th>
        <th>D5</th>
        <th>Game ID</th>
    </tr>

<c:forEach var="row" items="${result.rows}">
        <tr>
            <td>${row.Turn}</td>
            <td>${row.TurnScore}</td>
            <td>${row.ScoreType}</td>
            <td>${row.Name}</td>
            <td>${row.DieOne}</td>
            <td>${row.DieTwo}</td>
            <td>${row.DieThree}</td>
            <td>${row.DieFour}</td>
            <td>${row.DieFive}</td>
            <td>${row.GameID}</td>
</c:forEach>
</table>
