
        
var customGetPlayerStats = function (playerName) {

    var xHttp;

    if (playerName === "")
    {
        document.getElementById("player-stats-container").innerHTML = "";
        return;
    }
    xHttp = new XMLHttpRequest();
    xHttp.onreadystatechange = function ()
    {
        if (xHttp.readyState === 4 && xHttp.status === 200)
        {

            document.getElementById("player-stats-container").innerHTML = xHttp.responseText;

        }
    };
    xHttp.open("GET", "getPlayerStats.jsp?q=" + playerName, true);
    xHttp.send();
};

 
var customGetLooserStats = function (playerName) {

    var xHttp;

    if (playerName === "")
    {
        document.getElementById("looser-stats-container").innerHTML = "";
        return;
    }
    xHttp = new XMLHttpRequest();
    xHttp.onreadystatechange = function ()
    {
        if (xHttp.readyState === 4 && xHttp.status === 200)
        {

            document.getElementById("looser-stats-container").innerHTML = xHttp.responseText;

        }
    };
    xHttp.open("GET", "getLooserStats.jsp?q=" + playerName, true);
    xHttp.send();
};

$(document).ready(function () {
    customGetPlayerStats("Arnold");
    customGetLooserStats("Arnold");
    $("#player-name").text("Arnold");
    $("#winner-table-heading").text("Winners from " + "Arnold" + "'s games");
    $("#player-stats-container").slideUp(200).fadeIn(100);
    $("#looser-stats-container").slideUp(300).fadeIn(500);
});



$("#top-inputs").on("submit", function (event) {
    event.preventDefault();
    var answer = $("#text-input").val();
    customGetPlayerStats(answer);
    customGetLooserStats(answer);
    $("#player-name").text(answer);
    $("#winner-table-heading").text("Winners from " + answer + "'s games");
    document.getElementById("top-inputs").reset();
    $("#player-stats-container").slideUp(200).fadeIn(100);
    $("#looser-stats-container").slideUp(300).fadeIn(500);
});



       

