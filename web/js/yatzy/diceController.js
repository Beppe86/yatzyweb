
var rollAllDice = function () {

    for (var i = 0; i < Dice.length; i++)
    {
        Dice[i].roll();
    }
};

function Die(position)
{
    this.position = position;
    this.ShowDice = false;
    this.value;
}

Die.prototype = {
    roll: function () {
        if (!this.ShowDice)
        {
            this.value = Math.floor(Math.random() * 6) + 1;
            var diceImage = "<img src=" + "img/dice" + this.value + ".png>";
            var imageButtons = document.getElementsByClassName("dice-buttons");
            imageButtons[this.position].innerHTML = diceImage;
        }
    },
    toggle: function () {
        this.ShowDice = !this.ShowDice;
        var imageButtons = $(".dice-buttons");
        if (this.ShowDice)
            $(imageButtons[this.position]).css("opacity", "1");
        else
            $(imageButtons[this.position]).css("opacity", "0.3");
    },
    update: function () {
        var diceImage = "<img src=" + "img/dice" + this.value + ".png>";
        var imageButtons = document.getElementsByClassName("dice-buttons");
        imageButtons[this.position].innerHTML = diceImage;
    },
    setEnable: function (boolean)
    {
        if (boolean)
            this.ShowDice = false;
        else
            this.ShowDice = true;

        this.toggle();
    }
};

function Counter()
{
    this.counter = 0;
}

Counter.prototype = {
    increase: function ()
    {
        if (this.counter >= 3)
        {
            this.counter = 0;
        } else
        {
            this.counter++;
        }
    }
};

$(".dice-buttons").click(function ()
{
    if (DiceEnabled)
        Dice[$(this).val()].toggle();
});

$("#roll").on("click", function () {

    if (gameRunning)
    {
        Counter.increase();

        if (Counter.counter === 1)
        {
            for (var i = 0; i < Dice.length; i++)
            {
                var die = Dice[i];
                die.setEnable(false);
                die.roll();
                die.setEnable(true);
            }

            DiceEnabled = true;
            $(this).text("Roll " + Counter.counter);
        } else if (Counter.counter === 2)
        {
            for (var i = 0; i < Dice.length; i++)
            {
                var die = Dice[i];
                die.roll();
                die.setEnable(true);
            }

            $(this).text("Roll " + Counter.counter);
        } else if (Counter.counter === 3)
        {

            for (var i = 0; i < Dice.length; i++)
            {
                var die = Dice[i];
                die.roll();
                die.setEnable(true);
            }
            $("#score-table td:not(#labels):not(#score-cells").css("border-color", "greenyellow");
            DiceEnabled = false;
            gameRunning = false;
            $("#roll").text("Set your score!");
            $("#dice-bar").addClass("setScore");
            //$("#roll").text("Roll ");
        }
    }
});
var resetDice = function (dice)
{
    this.Dice = [];

    if (dice !== undefined)
    {
        for (var i = 0; i < dice.length; i++)
        {
            var die = new Die(i);
            die.value = dice[i];
            die.toggle();
            this.Dice.push(die);
            this.Dice[i].update();
        }

        gameRunning = false;
        DiceEnabled = false;
        //$("#roll").attr("disabled", true);
        $("#dice-bar").addClass("setScore");
        $("#roll").text("Set your score!");
        $("#roll").css("opacity", "1");
        this.Counter.counter = 3;

    } else
    {
        Dice = [new Die(0), new Die(1), new Die(2), new Die(3), new Die(4)];
        this.Counter = new Counter();
        $("#roll").css("opacity", "1");
    }
};
this.DiceEnabled = false;
