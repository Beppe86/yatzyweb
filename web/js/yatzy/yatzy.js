var checkGameState = function (GameState)
{
    if (GameState !== undefined)
    {
        setPlayersToList(GameState.names);

        if (GameState.gameRunning)
        {
            this.gameRunning = true;
            resetDice(GameState.dice);

        } else if (GameState.gameUploadable)
        {
            $("#upload-btn").prop("disabled", false);
            $("#upload-btn").css("opacity", "1");

        } else
        {
            $("#upload-btn").prop("disabled", true);
            $("#upload-btn").css("opacity", "0.3");
        }

        setScoreTable(GameState.players, GameState.currentPlayer);


    } else
    {
        resetDice();
    }
};

var setScoreTable = function (players, currentPlayer)
{
    $("#current-player-name").text(currentPlayer.name + "'s turn").addClass("blue-text");

    for (var i = 0; i < players.length; i++)
    {
        // create elements <table> and a <tbody>
        var tbl = $("#score-table");

        var tblTh = document.createElement("th");
        var name = players[i].name;
        var thText = document.createTextNode(name);
        tblTh.appendChild(thText);

        $("#score-table tr").eq(0).append(tblTh);

        for (var j = 0; j < players[i].webScore.length; j++)
        {
            var cell = document.createElement("td");
            cell.id = "score-cells";
            var point = "--";
            if (players[i].webScore[j] !== -1)
                point = players[i].webScore[j];

            var cellText = document.createTextNode(point);

            cell.appendChild(cellText);
            $("#score-table tr").eq(j + 1).append(cell);
        }
    }
};
var setPlayersToList = function (names)
{
    if (names !== undefined)
    {
        var $players = $("#player-list");

        for (var i = 0; i < names.length; i++)
        {
            $players.html($players.html() + names[i] + "<br />");
        }
    }
};

this.scoreType;

$(".labels--Clickabel").click(function ()
{
    if (Counter.counter === 3)
    {
        scoreType = $(this).attr("value");

        $("#dice-bar").submit();
    }
});

function DiceSubmit()
{
    var diceValue = [Dice[0].value, Dice[1].value, Dice[2].value, Dice[3].value, Dice[4].value];

    document.forms.diceBar.dice.value = diceValue;
    document.forms.diceBar.score_type.value = scoreType;

    document.forms.diceBar.submit();

    scoreType = null;
}