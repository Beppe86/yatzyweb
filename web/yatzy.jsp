<%-- 
    Document   : yatzy
    Created on : Mar 4, 2016, 3:28:32 PM
    Author     : Kermit
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
    <head>
        
        <link href='https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz' rel='stylesheet' type='text/css'>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/yatzy.css">
        <link rel="stylesheet" type="text/css" href="css/navlist.css">
        <title>Yatzy</title>
    </head>
    <body>

        <nav>
            <ul>
                <li><a href="yatzy.jsp">Play Game</a></li>
                <li><a href="top_players.html">Top Players</a></li>
                <li><a href="winners_loosers.html">Player Stats</a></li>
                <li><a href="game_stats.html">Game Stats</a></li>
            </ul>
        </nav>

        <!-- name input should be cleared after a player name submit -->

        <form id="settings-bar" action="Yatzy" method="GET">
            <input autocomplete="off" id="input-player" type="text" name="action" value="" placeholder="Player Name">
            <br />
            <button id="add-player" class="buttons" type="submit" name="action" value="add_player">Add Player</button>
            <br />
            <button id="start-btn" class="buttons" type="submit" name="action" value="start_game">Start Game</button>
            <br />
            <Button id="upload-btn" class="buttons" type="submit" name="action" value="upload_game">Upload Game</button>
            <br />
            <button id="reset-btn" class="buttons" type="submit" name="action" value="reset_game">Reset</button>
        </form>

        <div id="player-list"></div>

        <form id="dice-bar" name="diceBar" action="Yatzy" onsubmit="DiceSubmit();">
            <button id="dice-one" class="dice-buttons" name="first" type="button" value="0"><img src="img/dice1.png"></button>
            <button id="dice-two" class="dice-buttons" type="button" value="1"><img src="img/dice2.png"></button>
            <button id="dice-three" class="dice-buttons" type="button" value="2"><img src="img/dice3.png"></button>
            <button id="dice-four" class="dice-buttons" type="button" value="3"><img src="img/dice4.png"></button>
            <button id="dice-five" class="dice-buttons" type="button" value="4"><img src="img/dice5.png"></button>
            <input type="hidden" name="dice">
            <input type="hidden" name="score_type">
            <button id="roll" type="button" value="roll-dice">Roll</button>
        </form>

        <table id="score-table">
            <th id="current-player-name">PLAYER NAME</th>
            <tr>
                <td class="labels--Clickabel" value="1:">Aces:</td>
            </tr>
            <tr>
                <td class="labels--Clickabel" value="2:">Twos:</td>
            </tr>
            <tr>
                <td class="labels--Clickabel" value="3:">Threes:</td>
            </tr>
            <tr>
                <td class="labels--Clickabel" value="4:">Fours:</td>
            </tr>
            <tr>
                <td class="labels--Clickabel" value="5:">Fives:</td>
            </tr>
            <tr>
                <td class="labels--Clickabel" value="6:">Sixes:</td>
            </tr>
            <tr>
                <td id="labels" class="labels">UPPER TOTAL:</td>
            </tr>
            <tr>
                <td id="labels" class="labels">BONUS:</td>
            </tr>
            <tr>
                <td class="labels--Clickabel" value="Pair:">Pair:</td>
            </tr>
            <tr>
                <td class="labels--Clickabel" value="Two pairs:">Two pairs:</td>
            </tr>
            <tr>
                <td class="labels--Clickabel" value="Three of a kind:">Three of a kind:</td>
            </tr>
            <tr>
                <td class="labels--Clickabel" value="Small ladder:">Small ladder:</td>
            </tr>
            <tr>
                <td class="labels--Clickabel" value="Random:">Random:</td>
            </tr>
            <tr>
                <td class="labels--Clickabel" value="Large ladder:">Large ladder:</td>
            </tr>
            <tr>
                <td class="labels--Clickabel" value="Four of a kind:">Four of a kind:</td>
            </tr>
            <tr>
                <td class="labels--Clickabel" value="Full house:">Full house:</td>
            </tr>
            <tr>
                <td class="labels--Clickabel" value="Yatzy:">Yatzy:</td>
            </tr>
            <tr>
                <td id="labels" class="labels">GRAND TOTAL:</td>
            </tr>
            
            <audio id="fxbtn1" preload="auto">
                <source src="audio/ButtonOne.wav" type="audio/wav">
            </audio>
            <audio id="fxbtn2" preload="auto">
                <source src="audio/ButtonTwo.wav" type="audio/wav">
            </audio>
            <audio id="fxbtn3" preload="auto">
                <source src="audio/ButtonThree.wav" type="audio/wav">
            </audio>
            <audio id="fxroll1" preload="auto">
                <source src="audio/DiceRollOne.wav" type="audio/wav">
            </audio>
            <audio id="fxroll2" preload="auto">
                <source src="audio/DiceRollTwo.wav" type="audio/wav">
            </audio>
            <audio id="fxroll3" preload="auto">
                <source src="audio/DiceRollThree.wav" type="audio/wav">
            </audio>
            <audio id="fxroll4" preload="auto">
                <source src="audio/DiceRollFour.wav" type="audio/wav">
            </audio>
            <audio id="fxsave" preload="auto">
                <source src="audio/SaveDice.wav" type="audio/wav">
            </audio>
            <audio id="fxset1" preload="auto">
                <source src="audio/SetScore.wav" type="audio/wav">
            </audio>
            <audio id="fxset2" preload="auto">
                <source src="audio/SetScoreTwo.wav" type="audio/wav">
            </audio>
            <audio id="fxstart" preload="auto">
                <source src="audio/StartGame.wav" type="audio/wav">
            </audio>
       
         
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
            <script type="text/javascript" src="js/yatzy/yatzy.js"></script>
            <script type="text/javascript" src="js/yatzy/diceController.js"></script>
            <script type="text/javascript" src="js/yatzy/soundFx.js"></script>
            <script type="text/javascript">

                checkGameState(${gameState});

            </script>
    </body>
</html>