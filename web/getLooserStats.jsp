<%-- 
    Document   : getLooserStats
    Created on : Mar 3, 2016, 10:20:22 PM
    Author     : Kermit
--%>

<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>


<sql:setDataSource var="snapshot" driver="com.microsoft.sqlserver.jdbc.SQLServerDriver"
                   url="jdbc:sqlserver://62.101.44.83:1433;DatabaseName=KERPE"
                   user="KerpeAdmin"  password="kerpe"/>

<sql:query dataSource="${snapshot}" var="result">
    EXECUTE WebGetAllLoosersByName <%= request.getParameter("q") %>
</sql:query>


<table>
    <tr>
        <th style="width:100px">Player Name</th>
        <th>Total score</th>
        <th>Game ID</th>
    </tr>

<c:forEach var="row" items="${result.rows}">
        <tr>
            <td>${row.Name}</td>
            <td>${row.TotalScore}</td>
            <td>${row.GameID}</td>
</c:forEach>
</table>