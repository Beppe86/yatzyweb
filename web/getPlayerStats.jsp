<%-- 
    Document   : getPlayerStats
    Created on : Mar 3, 2016, 5:08:21 PM
    Author     : Kermit
--%>

<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>


<sql:setDataSource var="snapshot" driver="com.microsoft.sqlserver.jdbc.SQLServerDriver"
                   url="jdbc:sqlserver://62.101.44.83:1433;DatabaseName=KERPE"
                   user="KerpeAdmin"  password="kerpe"/>

<sql:query dataSource="${snapshot}" var="result">
    EXECUTE WebGetPlayerStatistics <%= request.getParameter("q")%>
</sql:query>


<table>
    <tr>
        <th>Game ID</th>
        <th style="width:100px">Player name</th>
        <th>Winner score</th>
        <th>Game date</th>
    </tr>

    <c:forEach var="row" items="${result.rows}">
        <c:set value="${gameId}${row.GameID}:" var="gameId"></c:set>
            <tr>
                <td>${row.GameID}</td>
                <td>${row.Name}</td>
                <td>${row.WinnerScore}</td>
                <td>${row.DateEnded}</td>
        </tr>
    </c:forEach>

</table>
