/*
Andreas Erlandsson 860802
beppe86@gmail.com

ClickSequencer
Mar 4, 2016
 */
package main.jsp;

import java.util.Arrays;
import main.dbcom.DbCom;
import main.dbcom.QueryBuilder;
import main.player.Player;
import main.system.CombinationTypes;
import main.system.DiceCup;
import main.system.DiceResult;
import main.system.GameMaster;
import main.system.GeneralScore;

/**
 *
 * @author Beppe
 */
public class ClickSequencer
{

    public static enum clickType
    {
        ADD_PLAYER, START_GAME, RESET_GAME, SCORE_SUBMIT, UPLOAD_GAME
    }

    GameMaster master;

    public ClickSequencer()
    {
        master = new GameMaster();
    }

    public void click(clickType click)
    {
        click(click, null);
    }

    public void click(clickType click, String input)
    {
        switch (click)
        {
            case ADD_PLAYER:
                addPlayer(input);
                break;

            case START_GAME:
                startGame();
                break;

            case RESET_GAME:
                resetGame();
                break;

            case SCORE_SUBMIT:
                submitScore(input);
                break;

            case UPLOAD_GAME:
                uploadGame();
                break;

            default:
                throw new AssertionError(click.name());
        }
    }

    private void submitScore(String input)
    {
        String[] parts = input.split(":");
        String[] diceStr = parts[1].split(",");

        GeneralScore score = null;
        for (int i = 0; i < CombinationTypes.values().length; i++)
        {
            if (parts[0].equals(CombinationTypes.values()[i].getName()))
            {
                score = CombinationTypes.values()[i];
                break;
            }
        }
        if (score == null)
        {
            score = DiceResult.values()[Integer.parseInt(parts[0]) - 1];
        }
        int[] dice = Arrays.asList(diceStr).stream().mapToInt(Integer::parseInt).toArray();

        DiceCup cup = master.getCup();
        cup.setDice(dice);

        int scoreValue = 0;

        if (score.getClass() == CombinationTypes.class)
        {
            scoreValue = cup.getTypeScore((CombinationTypes) score);
        } else
        {
            scoreValue = cup.getPlainScore(((DiceResult) score).getValue());
        }

        master.getCurrentPlayer().getScore().setScore(score, scoreValue, cup.getDiceResult());

        //SoundEffects.DICE.play();
        if (master.getSyncScore())
        {
            master.nextPlayer();

            //SoundEffects.RECEIVED.play();
        }
    }

    private void addPlayer(String name)
    {
        if (name.length() > 0 && !master.isGameRunning())
        {
            Player newPlayer = new Player(name);
            master.addPlayer(newPlayer);
        }
        //SoundEffects.ADDPLAYER.play();
    }

    private void startGame()
    {
        if (master.getPlayerNames().length > 0)
        {
            master.setGameRunning(true);
//            Player currentPlayer = master.getCurrentPlayer();

//            game.setCurrentPlayer(currentPlayer);
//            game.setGameRunning(true);
//            
//            return game;
        }
    }

    private void uploadGame()
    {
        if (master.isGameUploadable())
        {
            String query = QueryBuilder.getQuery(master.getAllPlayers());
            DbCom com = new DbCom(new String[]
            {
                query
            });
            Thread t = new Thread(com);
            t.start();
            master.setGameUploaded();
        }
    }

    private void resetGame()
    {
        master = new GameMaster();
    }

    public GameState getGameState()
    {
        GameState game = new GameState();
        game.setState(master);

        return game;
    }
}
