/*
Andreas Erlandsson 860802
beppe86@gmail.com

Yatzy
Mar 3, 2016
 */
package main.jsp;

import java.io.IOException;
import javax.servlet.RequestDispatcher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

/**
 *
 * @author Beppe
 */
public class Yatzy extends HttpServlet
{

    ClickSequencer clickSeq;

    public Yatzy()
    {
        super();
    }

    @Override
    public void init() throws ServletException
    {
        clickSeq = new ClickSequencer();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        GameState game = null;
        RequestDispatcher rd = request.getRequestDispatcher(("/yatzy.jsp"));

        String[] input = request.getParameterValues("action");
        if (input != null)
        {
            String action = input[1];

            if (null != action)
            {
                switch (action)
                {
                    case "add_player":
                        String name = input[0];
                        clickSeq.click(ClickSequencer.clickType.ADD_PLAYER, name);
                        break;

                    case "start_game":
                        clickSeq.click(ClickSequencer.clickType.START_GAME);
                        break;

                    case "reset_game":
                        clickSeq.click(ClickSequencer.clickType.RESET_GAME);
                        break;
                        
                    case "upload_game":
                        clickSeq.click(ClickSequencer.clickType.UPLOAD_GAME);
                    default:
                        break;
                }
            }
        } else
        {
            input = request.getParameterValues("dice");
            String score = request.getParameter("score_type") + input[0];
            clickSeq.click(ClickSequencer.clickType.SCORE_SUBMIT, score);
        }

        game = clickSeq.getGameState();

        request.setAttribute("gameState", new Gson().toJson(game));
        rd.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>

}
