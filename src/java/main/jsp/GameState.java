/*
Andreas Erlandsson 860802
beppe86@gmail.com

GameState
Mar 5, 2016
 */
package main.jsp;

import java.util.ArrayList;
import main.player.Player;
import main.system.GameMaster;

/**
 *
 * @author Beppe
 */
public class GameState
{

    private String[] names;

    private boolean showDiceBtn = false;
    private boolean showRollBtn = false;
    private int[] dice;

    private boolean gameRunning;
    private boolean gameUploadable;
    private Player currentPlayer;

    private Player[] players;

    public void setState(GameMaster master)
    {
        setPlayers(master);
        setDiceBar(master);

        gameRunning = master.isGameRunning();
        gameUploadable = master.isGameUploadable();

        if (gameRunning)
        {
            //master.getCurrentPlayer().getScore().getEntries() == master.getTurn()
            dice = master.getCup().getDiceValue();
        }
    }

    public void setDiceBar(GameMaster master)
    {
        showDiceBtn = master.isGameRunning();
        showRollBtn = master.isGameRunning();
        //Enable combo buttons in JSP
    }

    private void setPlayers(GameMaster master)
    {
        if (master.getCurrentPlayer() != null)
        {
            ArrayList<String> nameOutput = new ArrayList<>();
            ArrayList<Player> playerOutput = new ArrayList<>();

            Player player = master.getCurrentPlayer();

            do
            {
                player.setScore();
                nameOutput.add(player.getName());
                player = player.getNextPlayer();
            } while (player != master.getCurrentPlayer());

            player = master.getCurrentPlayer().getFirstPlayer();

            do
            {
                playerOutput.add(player);
                player = player.getNextPlayer();
            } while (player != master.getCurrentPlayer().getFirstPlayer());

            names = nameOutput.toArray(new String[nameOutput.size()]);
            players = playerOutput.toArray(new Player[playerOutput.size()]);
            currentPlayer = master.getCurrentPlayer();
        }
    }
}
