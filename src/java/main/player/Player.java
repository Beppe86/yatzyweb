/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 Player
 Oct 5, 2015
 */
package main.player;

import main.system.CombinationTypes;
import main.system.DiceResult;

/**
 *
 * @author Beppe
 */
public class Player
{

    private final Score score;
    private int[] webScore;

    private transient Player nextPlayer;

    private final String name;
    private boolean firstPlayer;

    public Player(String name)
    {
        this.name = name;
        score = new Score();
        webScore = new int[18];
        this.nextPlayer = this; //creates a cirkular loop for the playerList
    }

    public void setScore()
    {
        int i = 0;

        webScore[webScore.length - 1] = score.getTotalScore();

        for (DiceResult value : DiceResult.values())
        {
            if (score.getScore(value) != null)
            {
                webScore[i++] = score.getScore(value);
            } else
            {
                webScore[i++] = -1;
            }
        }

        webScore[i++] = score.getScore(true);

        if (score.isBonus())
        {
            webScore[i++] = 50;
        } else
        {
            webScore[i++] = 0;
        }

        for (CombinationTypes value : CombinationTypes.values())
        {
            if (score.getScore(value) != null)
            {
                webScore[i++] = score.getScore(value);
            } else
            {
                webScore[i++] = -1;
            }
        }
    }
    
    public Player getFirstPlayer()
    {
        if (firstPlayer)
            return this;
        else
        {
            return this.getNextPlayer().getFirstPlayer();
        }
    }

    public String getName()
    {
        return name;
    }

    public Score getScore()
    {
        return score;
    }

    public Player getNextPlayer()
    {
        return this.nextPlayer;
    }

    public void setFirstPlayer(boolean firstPlayer)
    {
        this.firstPlayer = firstPlayer;
    }

    public boolean isFirstPlayer()
    {
        return firstPlayer;
    }

    public void addPlayer(Player p)
    {
        if (nextPlayer == null)
        {
            nextPlayer = p;
        } else
        {
            p.nextPlayer = this.nextPlayer;
            this.nextPlayer = p;
        }
    }

}
