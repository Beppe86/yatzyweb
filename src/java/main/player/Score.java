/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 Score
 Oct 13, 2015
 */
package main.player;

import java.util.ArrayList;
import java.util.Iterator;
import main.system.CombinationTypes;
import main.system.DiceResult;
import main.system.GeneralScore;

/**
 *
 * @author Beppe
 */
public class Score
{

    private ArrayList<ScoreEntry> scores = new ArrayList<>();
    private boolean bonus;

    public void setScore(GeneralScore scoreType, int value, DiceResult[] dice)
    {
        boolean found = false;
        
        if (scores.stream().filter(ScoreEntry -> ScoreEntry.scoreType.equals(scoreType)).findFirst().isPresent())
        {
            found = true;
        }
        
        if (!found)
            scores.add(new ScoreEntry(scoreType, value, dice));
    }

    public Integer getScore(GeneralScore type)
    {
        Iterator<ScoreEntry> i = scores.iterator();

        while (i.hasNext())
        {
            ScoreEntry entry = i.next();
            if (entry.scoreType == type)
            {
                return entry.value;
            }
        }

        return null;
    }

    public int getTotalScore()
    {
        int sum = 0;

        sum += getScore(true);

        if (sum >= 63)
        {
            sum += 50;
            bonus = true;
        }

        sum += getScore(false);

        return sum;
    }

    public int getScore(boolean plainScore)
    {
        int sum = 0;

        Iterator<ScoreEntry> i = scores.iterator();

        while (i.hasNext())
        {
            ScoreEntry entry = i.next();

            if (plainScore)
            {
                if (entry.scoreType.getClass() == DiceResult.class)
                {
                    sum += entry.value;

                }
            } else if (entry.scoreType.getClass() == CombinationTypes.class)
            {
                sum += entry.value;
            }
        }

        return sum;
    }

    public boolean isBonus()
    {
        return bonus;
    }

    public void setBonus(boolean bonus)
    {
        this.bonus = bonus;
    }

    public int getEntries()
    {
        return scores.size();
    }

    public DiceResult[] getDices(int turn)
    {
        return scores.get(turn).dice;
    }

    public GeneralScore[] getScoreSequence()
    {
        GeneralScore[] scoreSequence = new GeneralScore[scores.size()];
        int index = 0;
        for (ScoreEntry entry : scores)
        {
            scoreSequence[index++] = entry.scoreType;
        }

        return scoreSequence;
    }
}
