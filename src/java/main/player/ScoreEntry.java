/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 ScoreEntry
 Feb 4, 2016
 */
package main.player;

import main.system.DiceResult;
import main.system.GeneralScore;

/**
 *
 * @author Beppe
 */
public class ScoreEntry
{

    public GeneralScore scoreType;
    public int value;
    public DiceResult[] dice;

    public ScoreEntry(GeneralScore type, int value, DiceResult[] dice)
    {
        this.scoreType = type;
        this.value = value;

        for (int i = 0; i < dice.length; i++)
        {
            for (int j = 1; j < dice.length - i; j++)
            {
                if (dice[j - 1].getValue() > dice[j].getValue())
                {
                    DiceResult tempDice = dice[j - 1];
                    dice[j - 1] = dice[j];
                    dice[j] = tempDice;
                }
            }
        }
        this.dice = dice;
    }
}
