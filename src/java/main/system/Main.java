/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 Main
 Oct 5, 2015
 */
package main.system;


import main.dbcom.GameGenerator;
import main.gui.Gui;
import main.gui.Statistics;

/**
 *
 * @author Beppe
 */
public class Main
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        //GameGenerator.generateGames();
        new Gui();
        new Statistics();
    }
    
}
