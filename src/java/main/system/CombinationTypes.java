/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 CombinationTypes
 Nov 20, 2015
 */
package main.system;

/**
 *
 * @author Beppe
 */
    public enum CombinationTypes implements GeneralScore
    {

        PAIR("Pair"), TWO_PAIRS("Two pairs"), THREE_OF_A_KIND("Three of a kind"),
        SMALL_LADDER("Small ladder"), RANDOM("Random"), LARGE_LADDER("Large ladder"), 
        FOUR_OF_A_KIND("Four of a kind"), FULL_HOUSE("Full house"), YATZY("Yatzy");

        private String name;

        private CombinationTypes(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
