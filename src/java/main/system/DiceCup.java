/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 DiceCup
 Oct 5, 2015
 */
package main.system;

/**
 *
 * @author Beppe
 */
public class DiceCup
{

    private Die die;
    private DiceResult[] dice = new DiceResult[5];
    private int currentNumberOfThrows = 0;
    private int dieIndex = 0;

    public DiceCup()
    {
        die = new Die();
    }

    public void resetCup()
    {
        dieIndex = 0;
        currentNumberOfThrows = 0;
        dice = new DiceResult[5];
    }
    
    public void setDice(int[] dice)
    {
        for (int i = 0; i < this.dice.length; i++)
        {
            this.dice[i] = DiceResult.values()[dice[i] - 1];
        }
    }

    public void throwDice()
    {
        dieIndex = 0;
        throwDie();
        currentNumberOfThrows++;
    }

    private void throwDie()
    {
        if (dieIndex < 5)
        {
            if (dice[dieIndex] == null)
            {
                dice[dieIndex] = die.throwDice();
            }
            dieIndex++;
            throwDie();
        }
    }

    public int getPlainScore(int dots)
    {
        int output = 0;
        for (DiceResult result : dice)
        {
            if (result.getValue() == dots)
            {
                output += dots;
            }
        }

        return output;
    }

    public int getTypeScore(CombinationTypes type)
    {
        int output = 0;

        switch (type)
        {
            case PAIR:
            {
                DiceResult maybePair = ofAKind(2, null);
                if (maybePair != null)
                {
                    output = maybePair.getValue() * 2;
                }
            }
            break;
            case TWO_PAIRS:
            {
                DiceResult maybePair = ofAKind(2, ofAKind(2, null));
                if (maybePair != null)
                {
                    output = (ofAKind(2, null).getValue() * 2) + (maybePair.getValue() * 2);
                }
            }
            break;
            case THREE_OF_A_KIND:
            {
                DiceResult maybeThree = ofAKind(3, null);
                if (maybeThree != null)
                {
                    output = maybeThree.getValue() * 3;
                }
            }
            break;
            case FOUR_OF_A_KIND:
            {
                DiceResult maybeFour = ofAKind(4, null);
                if (maybeFour != null)
                {
                    output = maybeFour.getValue() * 4;
                }
            }
            break;
            case SMALL_LADDER:
            {
                output = ladder();
                if (output != 15)
                {
                    output = 0;
                }
            }
            break;
            case LARGE_LADDER:
            {
                output = ladder();
                if (output != 20)
                {
                    output = 0;
                }
            }
            break;
            case FULL_HOUSE:
            {
                DiceResult maybeFullHouse1 = ofAKind(3, null);
                DiceResult maybeFullHouse2 = ofAKind(2, maybeFullHouse1);
                if (maybeFullHouse1 != null && maybeFullHouse2 != null)
                {
                    output = (maybeFullHouse1.getValue() * 3) + (maybeFullHouse2.getValue() * 2);
                }
            }
            break;
            case RANDOM:
            {
                output = sum();
            }
            break;
            case YATZY:
            {
                DiceResult maybeYatzee = ofAKind(5, null);
                if (maybeYatzee != null)
                {
                    output = 50;
                }
            }
            break;
            default:
                throw new AssertionError(type.name());
        }
        return output;
    }

    private int ladder()
    {
        insertionSort();

        int index = 0;

        while (index < 4)
        {
            if (dice[index + 1].getValue() - dice[index].getValue() != 1 )
            {
                return 0;
            }
            index++;
        }

        if (dice[index].getValue() == 6)
        {
            return 20;
        } else
        {
            return 15;
        }
    }

    private int sum()
    {
        int output = 0;

        for (DiceResult result : dice)
        {
            output += result.getValue();
        }

        return output;
    }

    private DiceResult ofAKind(int number, DiceResult alreadyTaken)
    {
        for (int i = dice.length; i >= 0; i--)
        {
            DiceResult assumedResult = DiceResult.values()[i];
            int counter = 0;
            for (DiceResult result : dice)
            {
                if (result == assumedResult && result != alreadyTaken)
                {
                    counter++;
                    if (counter == number)
                    {
                        return assumedResult;
                    }
                }
            }
        }
        return null;
    }

    private void insertionSort()
    {
        int in, out;
        {
            for (out = 1; out < dice.length; out++)
            {
                DiceResult temp = dice[out];
                in = out;

                while (in > 0 && dice[in - 1].getValue() > temp.getValue())
                {
                    dice[in] = dice[in - 1];
                    --in;
                }
                dice[in] = temp;
            }
        }
    }

    public int getCurrentNumberOfThrows()
    {
        return currentNumberOfThrows;
    }

    public DiceResult[] getDiceResult()
    {
        return dice;
    }
    
    public int[] getDiceValue()
    {
        int[] output = new int[5];
        
        if (dice[0] == null)
            return null;
        
        for (int i = 0; i < output.length; i++)
        {
            output[i] = dice[i].getValue();
        }
        
        return output;
    }
}
