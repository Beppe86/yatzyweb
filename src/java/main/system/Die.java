/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 Die
 Oct 6, 2015
 */
package main.system;

/**
 *
 * @author Beppe
 */
public class Die
{
    public static DiceResult throwDice()
    {
        int random = (int) (Math.random() * 6);
        return DiceResult.values()[random];
    }
}
