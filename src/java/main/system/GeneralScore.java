/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 GeneralScore
 Jan 27, 2016
 */
package main.system;

/**
 *
 * @author Beppe
 */
public interface GeneralScore
{
    public String getName();
}
