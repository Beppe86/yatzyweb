/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 DiceResult
 Nov 20, 2015
 */
package main.system;

/**
 *
 * @author Beppe
 */
public enum DiceResult implements GeneralScore
{

    ONE(1), TWO(2), THREE(3), FOUR(4), FIVE(5), SIX(6);

    private int value;

    private DiceResult(int value)
    {
        this.value = value;
    }

    public int getValue()
    {
        return value;
    }

    @Override
    public String getName()
    {
        return DiceResult.values()[value - 1].toString();

    }
}
