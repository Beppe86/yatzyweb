/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 GameMaster
 Oct 5, 2015
 */
package main.system;

import java.util.ArrayList;
import main.player.Player;

/**
 *
 * @author Beppe
 */
public class GameMaster
{

    private boolean gameRunning = false;

    private final DiceCup cup;
    private Player playerInList;
    private int nbrOfPlayer = 0;
    private int turn = 1;
    private boolean gameUploaded = false;

    public GameMaster()
    {
        cup = new DiceCup();
    }

    public void newDiceCup()
    {
        cup.resetCup();
    }

    public Player getCurrentPlayer()
    {
        return playerInList;
    }

    public void addPlayer(Player p)
    {
        if (playerInList == null)
        {
            p.setFirstPlayer(true);
            playerInList = p; //Beppe made this
        } else
        {
            Player temp = playerInList.getNextPlayer();
            while (temp.getNextPlayer() != playerInList)
            {
                temp = temp.getNextPlayer();
            }
            temp.addPlayer(p);
        }
        nbrOfPlayer++;
    }

    public void nextPlayer()
    {
        playerInList = playerInList.getNextPlayer();
        cup.resetCup();
        if (playerInList.isFirstPlayer())
        {
            turn++;
            if (turn > 15) //Checks if games is done
            {
                gameRunning = false;
            }
        }
    }

    public String[] getPlayerNames()
    {
        String[] output = new String[nbrOfPlayer];
        for (int i = 0; i < output.length; i++)
        {
            output[i] = playerInList.getName();
            playerInList = playerInList.getNextPlayer();
        }

        return output;
    }

    public Player[] getAllPlayers()
    {
        ArrayList<Player> output = new ArrayList<>();
        Player temp = playerInList;
        do
        {
            output.add(temp);
            temp = temp.getNextPlayer();
        } while (!temp.isFirstPlayer());

        return output.toArray(new Player[output.size()]);
    }

    public int getTurn()
    {
        return turn;
    }

    public boolean getSyncScore()
    {
        return (playerInList.getScore().getEntries() == turn);
    }

    public DiceCup getCup()
    {
        return cup;
    }

    public boolean isGameRunning()
    {
        return gameRunning;
    }

    public void setGameRunning(boolean gameRunning)
    {
        this.gameRunning = gameRunning;
    }

    public boolean isGameUploadable()
    {
        return (turn > 14 && !gameRunning && !gameUploaded);
    }

    public void setGameUploaded()
    {
        gameUploaded = true;
    }

}
