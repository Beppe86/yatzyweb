/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 ResourceStore
 Sep 16, 2015
 */
package main.gui;

import java.awt.AlphaComposite;
import java.awt.Composite;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import javax.imageio.ImageIO;
import main.system.DiceResult;

/**
 *
 * @author Beppe
 */
public class ResourceStore
{

    private static ResourceStore single = new ResourceStore();
    private static float scale = 0;

    public static ResourceStore get()
    {
        return single;
    }

    private HashMap<URL, Image> images = new HashMap();

    public Image getImage(DiceResult dieValue, float scale)
    {
        if (this.scale == 0)
        {
            this.scale = scale;
        }

        URL url = this.getClass().getClassLoader().getResource("main/resources/die_face_" + dieValue.getValue() + ".png");

        if (url == null) //debug
        {
            System.out.println("Can't load image");
            return null;
        }

        if (images.get(url) != null && this.scale == scale)
        {
            return images.get(url);
        }

        if (images.get(url) != null)
        {
            this.scale = scale;
            return resize(images.get(url), scale, false);
        }

        try
        {
            images.put(url, resize(ImageIO.read(new File(url.toURI())), scale, false));
            return getImage(dieValue);

        } catch (IOException ex)
        {
            System.out.println("Can't load image" + ex);
        } catch (URISyntaxException ex)
        {
            System.out.println("Error converting" + ex);
        }

        return null;
    }

    public Image getImage(DiceResult dieValue)
    {
        return getImage(dieValue, scale);
    }

    /**
     * Rescales image according to scale. have the option to pass image with
     * renderingHint antialiasing so the picture can have different alpha
     *
     * @param image picture to rescale
     * @param scale picture will be resized with this scale
     * @param transparent if true, pic will halve alpha 0.5
     * @return returns resized picture
     */
    private Image resize(Image image, float scale, boolean transparent)
    {
        int width = Math.round(image.getWidth(null) * scale);
        int height = Math.round(image.getHeight(null) * scale);

        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TRANSLUCENT); //maybe change?
        Graphics2D g2d = bi.createGraphics();
        if (!transparent)
        {
            g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY));
        } else
        {
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            Composite comp = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, (float) 0.5); //changes alpha to 50%
            g2d.setComposite(comp);
        }
        g2d.drawImage(image, 0, 0, width, height, null);
        g2d.dispose();

        return accImage(bi);
    }

    /**
     * With other programs I have written I have had some fps issues if I don't
     * use accelerated images so I do it in this project just in case. This
     * might need rework if we were to port the game to mobile
     *
     * @param sourceImage
     * @return accelerated graphics image
     */
    private Image accImage(Image sourceImage)
    {
        GraphicsConfiguration gc = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
        Image image = gc.createCompatibleImage(sourceImage.getWidth(null), sourceImage.getHeight(null), Transparency.BITMASK);

        image.getGraphics().drawImage(sourceImage, 0, 0, null);
        return image;
    }
    
    public static void setScale(float value)
    {
        scale = value;
    }
}
