/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 Gui
 Oct 6, 2015
 */
package main.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import main.system.DiceResult;
import main.system.DiceCup;
import main.system.GameMaster;
import main.player.Player;
//import main.soundfx.SoundEffects;

/**
 *
 * @author Beppe
 */
public class Gui extends JFrame
{

    GameMaster master;

    private JButton[] btnDice;
    private JButton btnRoll;
    private JLabel txtCurrentPlayer;
    private DefaultListModel nameList;
    private AL al = new AL();
    private CombinationMenu combiGui;
    private AddPlayerGui playerGui;

    public Gui()
    {
        this.master = new GameMaster();
        this.combiGui = new CombinationMenu(this, master);
        this.playerGui = new AddPlayerGui(this);
        ResourceStore.setScale(0.5f); //sets the scale to 50%

        windowConfig();
        soundFxStartProgram();
        this.add(diceBar(), BorderLayout.SOUTH);
        this.add(nameBar(master), BorderLayout.WEST);
        this.add(currentPlayerBar(), BorderLayout.NORTH);
        this.add(combiGui, BorderLayout.CENTER);
        //kermit
        combiGui.setEnabled(false);
        addActionOnClose();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        
    }
    
    
    private void windowConfig()
    {
        this.setSize(800, 550);
        this.setResizable(false);
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        this.setLayout(new BorderLayout());
    }
    
    //kermit
    private void addActionOnClose() {
    	this.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                quitConfirm();
            }
        });
    }
    
    //kermit
    public void quitConfirm() {
    	//SoundEffects.QUIT.play();
    	int quit = JOptionPane.showConfirmDialog(null, 
                "Sure you wanna quit dude?", "Quit!!..Seriously?", 
                	JOptionPane.YES_NO_OPTION,
                		JOptionPane.QUESTION_MESSAGE);
            if (quit == 0) {
            	//SoundEffects.DISCONNECT.play();
            	try {
					Thread.sleep(1400);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
            	System.exit(0);
            }
            return;
    }

    private JPanel nameBar(GameMaster master)
    {
        JPanel namePanel = new JPanel();
        namePanel.setLayout(new BorderLayout());

        namePanel.add(playerGui, BorderLayout.NORTH);

        nameList = new DefaultListModel();
        JList list = new JList(nameList);
        JScrollPane nameScroll = new JScrollPane(list);
        nameScroll.setPreferredSize(new Dimension(200, 300));
        namePanel.add(nameScroll, BorderLayout.CENTER);

        return namePanel;
    }

    private JPanel diceBar()
    {
        JPanel dicePanel = new JPanel();
        dicePanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
        dicePanel.setBackground(Color.BLACK);

        final int numberOfDice = 5;
        btnDice = new JButton[numberOfDice];

        for (int i = 0; i < btnDice.length; i++)
        {
            JButton btnNewDice = new JButton(new ImageIcon(ResourceStore.get().getImage(DiceResult.values()[i])));
            btnNewDice.setIcon(btnNewDice.getDisabledIcon());
            btnNewDice.addActionListener(al);
            btnNewDice.setEnabled(false);
            this.btnDice[i] = btnNewDice;

            dicePanel.add(this.btnDice[i]);
        }

        btnRoll = new JButton("Roll");
        Dimension d = btnDice[0].getPreferredSize();
        btnRoll.setPreferredSize(d);
        btnRoll.addActionListener(al);
        btnRoll.setEnabled(false);
        dicePanel.add(btnRoll);
        return dicePanel;

    }

    private JPanel currentPlayerBar()
    {
        JPanel currentPlayerPanel = new JPanel();
        txtCurrentPlayer = new JLabel("Now we need some players...");
        txtCurrentPlayer.setForeground(Color.red);
        Font font = new Font(Font.SANS_SERIF, Font.PLAIN, 30);
        txtCurrentPlayer.setFont(font);
        currentPlayerPanel.setBackground(Color.BLACK);
        currentPlayerPanel.add(txtCurrentPlayer);

        return currentPlayerPanel;
    }

    private void setBtnActionListeners(boolean setActionL)
    {
        for (JButton btnDie : btnDice)
        {
            if (setActionL)
            {
                btnDie.addActionListener(al);
            } else
            {
                btnDie.removeActionListener(al);
            }
        }
    }

    //--------------------
    private void updateDice(boolean reset)
    {
        for (int i = 0; i < btnDice.length; i++)
        {
            DiceResult result = master.getCup().getDiceResult()[i];
            JButton btnDie = btnDice[i];

            if (result != null)
            {
                Icon dieSide = new ImageIcon(ResourceStore.get().getImage(result));
                btnDie.setIcon(dieSide);
            } else if (reset)
            {
                btnDie.setIcon(btnDie.getDisabledIcon());
            }
        }
    }

    public void addPlayer(Player p)
    {
        nameList.clear();
        master.addPlayer(p);

        String[] names = master.getPlayerNames();

        for (String name : names)
        {
            nameList.addElement(name);
        }
    }

    public boolean startStopGame(boolean shouldGameRun)
    {
        if (master.getPlayerNames().length > 0)
        {
            for (JButton btn : btnDice)
            {
                btn.setEnabled(shouldGameRun);
                //kermit
                combiGui.setEnabled(shouldGameRun);
            }

            btnRoll.setEnabled(shouldGameRun);
            updateCurrentPlayer();

            return true;
        } else
        {
            return false;
        }
    }
    

    private void soundFxStartProgram() {
    	//SoundEffects.CONNECT.play();
    }
    
    private void updateCurrentPlayer()
    {
        txtCurrentPlayer.setText(master.getCurrentPlayer().getName() + " Turn " + master.getTurn());
        combiGui.updateScore();
    }

    class AL implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            for (int i = 0; i < btnDice.length; i++)
            {
                if (e.getSource() == btnDice[i])
                {
                	//SoundEffects.PRESSBUTTON.play();
                    if (master.getCup().getCurrentNumberOfThrows() > 0)
                    {
                        reverseButton(i);
                        updateDice(false);
                    }
                }
            }

            if (e.getSource() == btnRoll)
            {
                DiceCup cup = master.getCup();

                if (cup.getCurrentNumberOfThrows() < 3) //Player got more throws
                {
                	//SoundEffects.DICE.play();
                    cup.throwDice();
                    updateDice(false);
                    if (cup.getCurrentNumberOfThrows() >= 3)
                    {
                        setBtnActionListeners(false);
                        btnRoll.setText("Next Player");
                    }
                } else if (master.getSyncScore())
                {
                    if (master.getTurn() > 14) //Checks if games is done
                    {
                        startStopGame(false);
                        playerGui.enableBtn(true);
                    } else
                    {
                        master.nextPlayer();
                        //SoundEffects.RECEIVED.play();
                        updateCurrentPlayer();
                        updateDice(true);
                        btnRoll.setText("Roll");
                        setBtnActionListeners(true);
                    }

                }
            }
        }

        private void reverseButton(int index)
        {
            DiceResult result = master.getCup().getDiceResult()[index];

            if (result != null)
            {
                btnDice[index].setIcon(btnDice[index].getDisabledIcon());
                master.getCup().getDiceResult()[index] = null;
            } else
            {
                //Icon dieSide = new ImageIcon(ResourceStore.get().getImage(result));
                //btnDice[index].setIcon(dieSide);
            }
        }
        
    }
}
