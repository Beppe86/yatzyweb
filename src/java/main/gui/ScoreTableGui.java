package main.gui;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

import main.system.CombinationTypes;

public class ScoreTableGui extends JFrame {
	
	
	private JLabel[] lblLowlblScores = new JLabel[CombinationTypes.values().length];
	private JLabel[] lblScores = new JLabel[18];
	private JLabel lblPlayerName = new JLabel("  Score Table  ");
	private CombinationMenu cm;
	
	
	public ScoreTableGui(CombinationMenu cm) {
		this.cm = cm;
		setupWindow();
	
	}
	
	private void setupWindow() {
		
		JPanel pnlPlayer = new JPanel();
		JPanel pnlScoreLeft = new JPanel(new GridLayout(20, 1, 1, 1));
		JPanel pnlScoreRight = new JPanel(new GridLayout(20, 1, 1, 1));
		JLabel[] lblArray = new JLabel[8];
		EtchedBorder b = new EtchedBorder();
		
		lblArray[0] = new JLabel("Aces: ");
		lblArray[1] = new JLabel("Twos: ");
		lblArray[2] = new JLabel("Threes: ");
		lblArray[3] = new JLabel("Fours: ");
		lblArray[4] = new JLabel("Fives: ");
		lblArray[5] = new JLabel("Sixes: ");
		lblArray[6] = new JLabel("TOTAL: ");
		lblArray[7] = new JLabel("BONUS: ");
		lblArray[6].setFont(new Font(null, Font.BOLD, 14));
		lblArray[7].setFont(new Font(null, Font.BOLD, 14));
		
		
		for(int i = 0; i < lblArray.length; i++) {
			lblArray[i].setBorder(b);
			pnlScoreLeft.add(lblArray[i]);
		}
		
		for (int i = 0; i < lblLowlblScores.length; i++) {
            CombinationTypes type = CombinationTypes.values()[i];
            JLabel lbl = new JLabel();
            lbl.setBorder(b);
            lbl.setText(type.getName() + ": ");
            pnlScoreLeft.add(lbl);
		}
	
		
		lblPlayerName.setFont(new Font(null, Font.BOLD, 16));
		lblPlayerName.setHorizontalTextPosition(JLabel.CENTER);
		pnlPlayer.add(lblPlayerName);
		
		JLabel lblTotal = new JLabel("GRAND TOTAL:      ");
		lblTotal.setBorder(b);
		lblTotal.setFont(new Font(null, Font.BOLD, 14));
		pnlScoreLeft.add(lblTotal);
		
		
		for(int i = 0; i < lblScores.length; i++) {
			lblScores[i] = new JLabel();
			lblScores[i].setBorder(b);
			lblScores[i].setForeground(Color.BLUE);
			pnlScoreRight.add(lblScores[i]);
		}
		
		
		this.add(pnlPlayer, BorderLayout.NORTH);
		this.add(pnlScoreLeft, BorderLayout.WEST);
		this.add(pnlScoreRight, BorderLayout.CENTER);
		this.setSize(200, 600);
		this.setResizable(false);
		//this.setLocationRelativeTo(gui);
		this.setLocation(200,300);
		this.setVisible(true);
	}
	
	public void scoreAdder(int index, int score) {
		if(index >= 0 && index < 6 || index >= 8 && index < 17 )
			lblScores[index].setText(" " + score);
		
	}
	
	public void setPlainScore(int plainScore) {
		lblScores[6].setText(""+ plainScore);
	}
	
	public void setBonus(int bonus) {
		lblScores[7].setText("" + bonus);
	}
	
	public void setTotalScore(int totalScore) {
		lblScores[17].setText("" + totalScore);
	}
	
	public void setPlayerName(String name) {
		lblPlayerName.setText("   " + name + "'s scores   ");
	}
	
	public void clearScoreTable() {
		for(int i = 0; i < lblScores.length; i++)
			lblScores[i].setText("");
	}

}
