/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 CombinationMenu
 Oct 12, 2015
 */
package main.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
 import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;
import main.system.CombinationTypes;
import main.system.DiceResult;
import main.system.GameMaster;
import main.player.Score;
//import main.soundfx.SoundEffects;
import main.system.DiceCup;

/**
 *
 * @author Beppe
 */
public class CombinationMenu extends JPanel
{

    private JButton[] btnOneToSix = new JButton[DiceResult.values().length];
    private JButton[] btnCombiTypes = new JButton[CombinationTypes.values().length];
    
    private AL al = new AL();

    private Gui gui;
    private GameMaster master;
    private ScoreTableGui scoreTable;

    boolean clickable = true;

    public CombinationMenu(Gui gui, GameMaster master)
    {
        this.gui = gui;
        this.master = master;
        scoreTable = new ScoreTableGui(this);

        this.setLayout(new BorderLayout());
        this.add(leftSide(), BorderLayout.CENTER);
        this.add(rightSide(), BorderLayout.EAST);
    }

    private JPanel getOneToSixBar()
    {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(3, 2));

        for (int i = 0; i < btnOneToSix.length; i++)
        {
            JButton btn = new JButton(DiceResult.values()[i].toString());
            btn.setPreferredSize(new Dimension(100, 100));
            btnOneToSix[i] = btn;
            panel.add(btn);
        }

        return panel;
    }

    private JPanel leftSide()
    {
        JPanel leftSide = new JPanel();
        leftSide.setLayout(new BorderLayout());
        //Font font = new Font(Font.SANS_SERIF, Font.PLAIN, 10);
        //lblOneToSix.setFont(font);

        leftSide.add(getOneToSixBar(), BorderLayout.CENTER);
        //leftSide.add(lblOneToSix, BorderLayout.SOUTH);
        return leftSide;
    }

    private JPanel rightSide()
    {
        JPanel rightSide = new JPanel();
        rightSide.setLayout(new GridLayout(3, 3));

        for (int i = 0; i < CombinationTypes.values().length; i++)
        {
            JButton btn = new JButton(CombinationTypes.values()[i].getName());
            btnCombiTypes[i] = btn;
            rightSide.add(btn, BorderLayout.CENTER);
        }
//        rightSide.add(btnPair);
//        rightSide.add(btnTwoPairs);
//        rightSide.add(btnTriplets);
//        rightSide.add(btnFours);
//        rightSide.add(btnSmallLadder);
//        rightSide.add(btnLargeLadder);
//        rightSide.add(btnFullHouse);
//        rightSide.add(btnRandom);
//        rightSide.add(btnYatzy);

        return rightSide;
    }
  //kermit  
    public void setEnabled(boolean tof) {
    	for (int i = 0; i < btnCombiTypes.length; i++)
    		btnCombiTypes[i].setEnabled(tof);
    	for (int i = 0; i < btnOneToSix.length; i++)
    		btnOneToSix[i].setEnabled(tof);
    }

    public void updateScore()
    {
        Score score = master.getCurrentPlayer().getScore();
        clickable = true;
        scoreTableInit();
        

        int plainPoints = 0;
        for (int i = 0; i < btnOneToSix.length; i++)
        {
            Integer p = score.getScore(DiceResult.values()[i]);
            if (p != null)
            {
                //btnOneToSix[i].removeActionListener(al);
                //btnOneToSix[i].setText(DiceResult.values()[i].toString() + " " + p + " points");
                scoreTable.scoreAdder(i, p);
                plainPoints += p;
            } else
            {
                btnOneToSix[i].setText(DiceResult.values()[i].toString());
                btnOneToSix[i].addActionListener(al);

            }
        }
        for (int i = 0; i < btnCombiTypes.length; i++)
        {
            CombinationTypes type = CombinationTypes.values()[i];
            JButton btn = btnCombiTypes[i];
            Integer typeScore = score.getScore(type);

            if (typeScore != null)
            {
                //btn.removeActionListener(al);
                //btn.setText(type.getName() + " " + typeScore + " points");
                scoreTable.scoreAdder(i + 8, typeScore);
            } else
            {
                btn.addActionListener(al);
                btn.setText(CombinationTypes.values()[i].getName());
            }
        }

//        if (score.getScore(CombinationTypes.PAIR) != null)
//        {
//            btnPair.removeActionListener(al);
//            btnPair.setText(CombinationTypes.PAIR.getName() + " " + score.getScore(CombinationTypes.PAIR).getValue() + " points");
//        }
//        else
//        {
//            btnPair.setText(CombinationTypes.PAIR.getName());
//            btnPair.addActionListener(al);
//        }
        //lblOneToSix.setText("You got " + String.format("%02d", plainPoints) + " of the needed 63 Points to get the bonus");
    }
    
    private void scoreTableInit() {
    	
    	scoreTable.clearScoreTable();
    	scoreTable.setBonus(0);
        scoreTable.setPlayerName(master.getCurrentPlayer().getName());
        scoreTable.setPlainScore(master.getCurrentPlayer().getScore().getScore(true));
        if(master.getCurrentPlayer().getScore().isBonus()) {
        	scoreTable.setBonus(63);
        }
        scoreTable.setTotalScore(master.getCurrentPlayer().getScore().getTotalScore());
    }

    class AL implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            if (clickable)
            {
                if (master.getCup().getCurrentNumberOfThrows() == 3)
                {
                    for (int i = 0; i < btnCombiTypes.length; i++)
                    {
                        if (e.getSource() == btnCombiTypes[i])
                        {
                            CombinationTypes type = CombinationTypes.values()[i];
                            DiceCup cup = master.getCup();
                            master.getCurrentPlayer().getScore().setScore(type, cup.getTypeScore(type), cup.getDiceResult());
                            //SoundEffects.SETSCORE.play();
                            updateScore();
                            clickable = false;
                        }
                    }

                    for (int i = 0; i < btnOneToSix.length; i++)
                    {   
                        if (e.getSource() == btnOneToSix[i])
                        {
                            DiceResult dots = DiceResult.values()[i];
                            DiceCup cup = master.getCup();
                            master.getCurrentPlayer().getScore().setScore(dots, cup.getPlainScore(dots.getValue()), cup.getDiceResult());
                            //SoundEffects.SETSCORE.play();
                            updateScore();
                            clickable = false;
                        }
                    }
                }
            }
        }
    }
}
