/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 AddPlayerGui
 Oct 8, 2015
 */
package main.gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import main.dbcom.DbCom;
import main.dbcom.QueryBuilder;
import main.player.Player;
//import main.soundfx.SoundEffects;

/**
 *
 * @author Beppe
 */
public class AddPlayerGui extends JPanel implements ActionListener, KeyListener
{

    private final JButton btnAddPlayer = new JButton("Add Player");
    private final JButton btnStartGame = new JButton("Start Game");
    private final JButton btnUpload = new JButton("Upload Game");
    private final JButton btnQuitGame = new JButton("Quit Game");
    private final JButton btnStatistics = new JButton("Statistics");
    private final JTextField txtName = new JTextField();

    private Gui gui;
    private Statistics stats;
    //private DbCom com;

    public AddPlayerGui(Gui gui)
    {
    	
    	//com = new DbCom();
    	stats = new Statistics();
        this.gui = gui;
        btnUpload.setEnabled(true); //true just for test purpose

        btnAddPlayer.addActionListener(this);
        btnStartGame.addActionListener(this);
        btnUpload.addActionListener(this);
        btnStatistics.addActionListener(this);
        btnQuitGame.addActionListener(this);
        txtName.addKeyListener(this);

        //kermit
        this.setLayout(new GridLayout(6, 1));
        this.add(txtName);
        this.add(btnAddPlayer);
        this.add(btnStartGame);
        this.add(btnUpload);
        this.add(btnStatistics);
        this.add(btnQuitGame);
        
    }

    public void enableBtn(boolean b) //SNYGGT!!!
    {
        btnAddPlayer.setEnabled(b);
        btnStartGame.setEnabled(b);
        btnUpload.setEnabled(b);
        btnQuitGame.setFocusable(b);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() == btnAddPlayer)
        {
            if (!txtName.getText().isEmpty())
            {
            	//SoundEffects.ADDPLAYER.play();
                gui.addPlayer(new Player(txtName.getText()));
            }
            txtName.setText("");
        } else if (e.getSource() == btnStartGame)
        {
            if (gui.startStopGame(true))
            {
            	//SoundEffects.PRESSBUTTON.play();
                enableBtn(false);
                btnUpload.setEnabled(true);
            }
        } 
        else if (e.getSource() == btnUpload)
        {
            String query = QueryBuilder.getQuery(gui.master.getAllPlayers());
            DbCom.insertIntoDb(new String[]{query});
        }
        
        else if (e.getSource() == btnQuitGame)
        {
            gui.quitConfirm();
            {
            	return;
            }
        }else if(e.getSource() == btnStatistics) {
        	stats.showStatsWindow(true);
        }
    }

    @Override
    public void keyTyped(KeyEvent e)
    {

    }

    @Override
    public void keyPressed(KeyEvent e)
    {
        if (e.getKeyCode() == KeyEvent.VK_ENTER)
        {
            btnAddPlayer.doClick();
        }

    }

    @Override
    public void keyReleased(KeyEvent e)
    {

    }

}
