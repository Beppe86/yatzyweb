package main.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import main.dbcom.DbCom;
import main.dbcom.QueryBuilder;

public class Statistics extends JFrame implements ActionListener {

	private JButton btnName;
	private JButton btnGameId;
	private JButton btnTop20;
	private JButton btnTop100;
	private JTextField txtName;
	private JTextField txtGameId;
	private JTextArea txaStatsWindow;
	
	
	
	public Statistics() {
		this.setTitle("Game Statistics");
		windowSetup();
		actionSetup();
	}
	
	
	private void windowSetup() {
		
		btnName = new JButton("Search by name");
		btnGameId = new JButton("Seach by gameID");
		txtName = new JTextField("Input player name");
		txtGameId = new JTextField("Input game ID");
		btnTop20 = new JButton("Top 20 players");
		btnTop100 = new JButton("Top 100 players");
		txaStatsWindow = new JTextArea();
		
		JScrollPane scrStats = new JScrollPane(txaStatsWindow);
		scrStats.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		JPanel pnlInputs = new JPanel(new GridLayout(3, 2));
		
		
		pnlInputs.add(txtName);
		pnlInputs.add(txtGameId);
		pnlInputs.add(btnName);
		pnlInputs.add(btnGameId);
		pnlInputs.add(btnTop20);
		pnlInputs.add(btnTop100);
		
		pnlInputs.setSize(400,  80);
		
		
		this.add(pnlInputs, BorderLayout.NORTH);
		this.add(scrStats);
		//this.add(btnHiScore, BorderLayout.SOUTH);
	
		this.setSize(600, 700);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(false);
		
	}
	
	private void actionSetup() {
		
		btnName.addActionListener(this);
		btnGameId.addActionListener(this);
		 
		txtName.addActionListener(this);
		txtGameId.addActionListener(this);
		
		btnTop20.addActionListener(this);
		btnTop100.addActionListener(this);
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {

		if(e.getSource() == btnName) {
			
			
			
			txtName.setText("");
			
		}else if(e.getSource() == btnGameId) {
			
			
			
			txtGameId.setText("");

		}else if(e.getSource() == btnTop20) {
			
			clearScreen();
			txaStatsWindow.setText(DbCom.getTopScores(QueryBuilder.getTop20()));
			DbCom.disconnect();
			
		}else if(e.getSource() == btnTop100) {
			
			clearScreen();
			txaStatsWindow.setText(DbCom.getTopScores(QueryBuilder.getTop100()));
			DbCom.disconnect();
			
		}
			
	}
	
	public void showStatsWindow(Boolean show) {
		this.setVisible(show);
	}
	
	public void displayTxt(String text) {
		txaStatsWindow.setText(text);
	}
	
	private void clearScreen() {
		txaStatsWindow.setText("");
	}

}
