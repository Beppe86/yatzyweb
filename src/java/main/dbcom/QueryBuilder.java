/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 QueryBuilder
 Jan 27, 2016
 */
package main.dbcom;

import main.player.Player;
import main.system.GeneralScore;

/**
 *
 * @author Beppe
 */
public class QueryBuilder
{

    public static String getQuery(Player[] players)
    {
        String output = startStatement();
        output += createGame();

        Player winner = players[0]; //assumes player 0 might have highest score

        for (int i = 0; i < players.length; i++)
        {
            Player singlePlayer = players[i];

            output += insertPlayer(singlePlayer);

            for (int j = 0; j < 15; j++)
            {
                output += createScoreEntry(singlePlayer, j);
            }

            if (singlePlayer.getScore().getTotalScore() > winner.getScore().getTotalScore())
            {
                winner = singlePlayer;
            }
        }
        output += finalizeScore(winner);

        return output;
    }

    private static String startStatement()
    {
        return "BEGIN TRY\n" + "BEGIN TRANSACTION\n";
    }

    private static String createGame()
    {
        return "EXECUTE CreateGame\n";
    }

    private static String insertPlayer(Player player)
    {
        int bonus = 0;
        int totalScore = player.getScore().getTotalScore();

        if (player.getScore().isBonus())
        {
            bonus = 1;
        }

        String output = "EXECUTE CreatePlayersInAGame '";
        output += player.getName();
        output += "', ";
        output += bonus;
        output += " ,";
        output += totalScore;
        return output + "\n";
    }

 private static String createScoreEntry(Player player, int turn)
    {
        GeneralScore type = player.getScore().getScoreSequence()[turn];
        String output = "EXECUTE CreateScore @ScoreType = '";
        output += type.getName();
        output += "', @TurnScore = ";
        output += player.getScore().getScore(type);
        output += ", @DieOne = ";
        output += player.getScore().getDices(turn)[0].getValue();
        output += ", @DieTwo = ";
        output += player.getScore().getDices(turn)[1].getValue();
        output += ", @DieThree = ";
        output += player.getScore().getDices(turn)[2].getValue();
        output += ", @DieFour = ";
        output += player.getScore().getDices(turn)[3].getValue();
        output += ", @DieFive = ";
        output += player.getScore().getDices(turn)[4].getValue();
        return output + "\n";
    }

    private static String finalizeScore(Player player)
    {
        String output = "EXECUTE FinalizeWinnerData '";
        output += player.getName();
        output += "', ";
        output += player.getScore().getTotalScore();
        output += "\nCOMMIT TRANSACTION\nEND TRY\nBEGIN CATCH\n";
        output += "IF @@TRANCOUNT > 0\nROLLBACK TRANSACTION\nTHROW;\nEND CATCH;";

        return output;
    }

    public static String getTop100()
    {

        String sql = "SELECT TOP(100) Name, WinnerScore"
                + " FROM Games"
                + " LEFT JOIN PlayersInAGame"
                + " ON GameID = Game_ID"
                + " LEFT JOIN Names"
                + " ON Name_ID = NameID"
                + " ORDER BY WinnerScore DESC";
        return sql;
    }

    public static String getTop20()
    {

        String sql = "SELECT TOP(20) Name, WinnerScore"
                + " FROM Games"
                + " LEFT JOIN PlayersInAGame"
                + " ON GameID = Game_ID"
                + " LEFT JOIN Names"
                + " ON Name_ID = NameID"
                + " ORDER BY WinnerScore DESC";
        return sql;
    }
}
