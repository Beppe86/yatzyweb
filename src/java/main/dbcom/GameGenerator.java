/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 GameGenerator
 Jan 28, 2016
 */
package main.dbcom;

import java.util.Random;
import main.player.Player;
import main.system.CombinationTypes;
import main.system.DiceResult;
import main.system.Die;

/**
 *
 * @author Beppe
 */
public class GameGenerator
{

    //Statistics
    //Plains
    private static final double SINGLETON = 0.079;
    private static final double PAIR = 25.601;
    private static final double THREE_OF_A_KIND = 45.240;
    private static final double FOUR_OF_A_KIND = 24.476;
    private static final double YATZY = 4.603;

    //Combos
    private static final double CBO_YATZY = 6;
    private static final double CBO_FOUR_OF_A_KIND = 150 + CBO_YATZY;
    private static final double CBO_FULL_HOUSE = 300 + CBO_FOUR_OF_A_KIND;
    private static final double CBO_LADDER = 720 + CBO_FULL_HOUSE;
    private static final double CBO_THREE_OF_A_KIND = 1200 + CBO_LADDER;
    private static final double CBO_TWO_PAIRS = 1800 + CBO_THREE_OF_A_KIND;
    private static final double CBO_PAIR = 3600 + CBO_TWO_PAIRS;

    public static void generateGames()
    {
        int nbrOfGames = 200;
        String[] games = new String[nbrOfGames];

        for (int i = 0; i < nbrOfGames; i++)
        {
            games[i] = QueryBuilder.getQuery(generatePlayers());
        }
        DbCom.insertIntoDb(games);
    }

    private static Player[] generatePlayers()
    {
        Player[] players;

        int nbrPlayers = (int) (Math.random() * 9) + 1;
        players = new Player[nbrPlayers];

        for (int i = 0; i < nbrPlayers; i++)
        {
            players[i] = new Player(NameBank.getName());
            generatePlains(players[i]);
            generateCombo(players[i]);
        }
        return players;
    }

    private static void generatePlains(Player player)
    {
        double roll;
        Random rnd = new Random();

        for (int i = 0; i < DiceResult.values().length; i++)
        {
            roll = rnd.nextDouble() * 100;
            if (roll <= SINGLETON)
            {
                setPlainScore(i, 1, player);
            } else if (roll <= SINGLETON + PAIR)
            {
                setPlainScore(i, 2, player);
            } else if (roll <= SINGLETON + PAIR + THREE_OF_A_KIND)
            {
                setPlainScore(i, 3, player);
            } else if (roll <= SINGLETON + PAIR + THREE_OF_A_KIND + FOUR_OF_A_KIND)
            {
                setPlainScore(i, 4, player);
            } else
            {
                setPlainScore(i, 5, player);
            }
        }
    }

    private static void generateCombo(Player player)
    {
        Boolean[] combos = new Boolean[CombinationTypes.values().length];
        recComboRoll(combos, 0);

        for (int i = 0; i < combos.length; i++)
        {
            if (combos[i])
            {
                setComboScore(CombinationTypes.values()[i], player);
            } else
            {
                player.getScore().setScore(CombinationTypes.values()[i], 0, getZeroPointRoll());
            }
        }
    }

    private static void recComboRoll(Boolean[] combos, int turn)
    {
        if (turn > 9)
        {
            return;
        }
        int chanceRoll = 7776;
        for (int i = 0; i < 3; i++)
        {
            int temp = (int) (Math.random() * 7776) + 1;
            if (temp < chanceRoll)
            {
                chanceRoll = temp;
            }
        }

        if (chanceRoll <= CBO_YATZY && combos[CombinationTypes.YATZY.ordinal()] == null)
        {
            combos[CombinationTypes.YATZY.ordinal()] = true;
        } else if (chanceRoll <= CBO_FOUR_OF_A_KIND && combos[CombinationTypes.FOUR_OF_A_KIND.ordinal()] == null)
        {
            combos[CombinationTypes.FOUR_OF_A_KIND.ordinal()] = true;
        } else if (chanceRoll <= CBO_FULL_HOUSE && combos[CombinationTypes.FULL_HOUSE.ordinal()] == null)
        {
            combos[CombinationTypes.FULL_HOUSE.ordinal()] = true;
        } else if (chanceRoll <= CBO_LADDER
                && ((combos[CombinationTypes.LARGE_LADDER.ordinal()] == null)
                || combos[CombinationTypes.SMALL_LADDER.ordinal()] == null))
        {
            if (combos[CombinationTypes.LARGE_LADDER.ordinal()] == null)
            {
                combos[CombinationTypes.LARGE_LADDER.ordinal()] = true;
            } else
            {
                combos[CombinationTypes.SMALL_LADDER.ordinal()] = true;
            }
        } else if (chanceRoll <= CBO_THREE_OF_A_KIND && combos[CombinationTypes.THREE_OF_A_KIND.ordinal()] == null)
        {
            combos[CombinationTypes.THREE_OF_A_KIND.ordinal()] = true;
        } else if (chanceRoll <= CBO_TWO_PAIRS && combos[CombinationTypes.TWO_PAIRS.ordinal()] == null)
        {
            combos[CombinationTypes.TWO_PAIRS.ordinal()] = true;
        } else if (chanceRoll <= CBO_PAIR && combos[CombinationTypes.PAIR.ordinal()] == null)
        {
            combos[CombinationTypes.PAIR.ordinal()] = true;
        } else if (combos[4] == null) //checks Random
        {
            combos[4] = true;
        } else
        {
            for (int i = 0; i < CombinationTypes.values().length; i++)
            {
                if (combos[i] == null)
                {
                    combos[i] = false;
                    break;
                }
            }
        }

        recComboRoll(combos, ++turn);
    }

    private static void setPlainScore(int i, int multiplier, Player player)
    {
        int k = multiplier;
        DiceResult dots = DiceResult.values()[i];
        DiceResult[] dice = new DiceResult[5];
        DiceResult oldDie = dots;
        DiceResult veryOldDie = dots;

        for (int j = 0; j < dice.length; j++)
        {
            while (k > 0)
            {
                dice[j++] = dots;
                k--;
            }

            if (j < 5 && dice[j] == null)
            {
                DiceResult die;
                do
                {
                    die = Die.throwDice();
                } while (die.getValue() == i + 1 || oldDie == die || veryOldDie == die);

                dice[j] = die;
                veryOldDie = oldDie;
                oldDie = die;
            }
        }

        player.getScore().setScore(dots, dots.getValue() * multiplier, dice);
    }

    private static void setComboScore(CombinationTypes type, Player player)
    {
        int dieRoll = DiceResult.values()[(int) (Math.random() * 3) + 3].getValue();
        int altDieRoll;
        do
        {
            altDieRoll = DiceResult.values()[(int) (Math.random() * 4) + 2].getValue();
        } while (altDieRoll == dieRoll);

        switch (type)
        {
            case PAIR:
                player.getScore().setScore(type, dieRoll * 2, getDices(type, dieRoll, altDieRoll, 2));
                break;
            case TWO_PAIRS:
                player.getScore().setScore(type, (dieRoll * 2) + (altDieRoll * 2), getDices(type, dieRoll, altDieRoll, 2));
                break;
            case THREE_OF_A_KIND:
                player.getScore().setScore(type, dieRoll * 3, getDices(type, dieRoll, altDieRoll, 3));
                break;
            case FOUR_OF_A_KIND:
                player.getScore().setScore(type, dieRoll * 4, getDices(type, dieRoll, altDieRoll, 4));
                break;
            case YATZY:
                player.getScore().setScore(type, 50, getDices(type, dieRoll, altDieRoll, 5));
                break;
            case FULL_HOUSE:
                player.getScore().setScore(type, (dieRoll * 3) + (altDieRoll * 2), getDices(type, dieRoll, altDieRoll, 3));
                break;
            case LARGE_LADDER:
                player.getScore().setScore(type, 20, getDices(type, dieRoll, altDieRoll, 0));
                break;
            case SMALL_LADDER:
                player.getScore().setScore(type, 15, getDices(type, dieRoll, altDieRoll, 0));
                break;
            case RANDOM:
            {
                int sum = 0;
                DiceResult[] dice = new DiceResult[5];
                for (int i = 0; i < 5; i++)
                {
                    dice[i] = DiceResult.values()[(int) (Math.random() * 6)];
                    sum += dice[i].getValue();
                }
                player.getScore().setScore(type, sum, dice);
                break;
            }

        }
    }

    private static DiceResult[] getZeroPointRoll()
    {
        return new DiceResult[]
        {
            DiceResult.ONE, DiceResult.TWO, DiceResult.THREE, DiceResult.FOUR, DiceResult.SIX
        };
    }

    private static DiceResult[] getDices(CombinationTypes type, int roll1, int roll2, int multiplier)
    {
        DiceResult dieRoll = DiceResult.values()[roll1 - 1];
        DiceResult altDieRoll = DiceResult.values()[roll2 - 1];

        DiceResult[] dice = new DiceResult[5];

        for (int i = 0; i < dice.length; i++)
        {
            if (type == CombinationTypes.LARGE_LADDER)
            {
                dice[i] = DiceResult.values()[i + 1];
            } else if (type == CombinationTypes.SMALL_LADDER)
            {
                dice[i] = DiceResult.values()[i];
            } else
            {
                while (multiplier > 0)
                {
                    dice[i++] = dieRoll;
                    multiplier--;
                }

                if (type == CombinationTypes.FULL_HOUSE)
                {
                    dice[i] = altDieRoll;
                } else if (type == CombinationTypes.TWO_PAIRS)
                {
                    dice[i++] = altDieRoll;
                }

                if (i < 5 && dice[i] == null)
                {
                    DiceResult finalDice;
                    do
                    {
                        finalDice = Die.throwDice();

                    } while (finalDice == dieRoll || finalDice == altDieRoll || finalDice == dice[2] || finalDice == dice[3]);

                    dice[i] = finalDice;
                }
            }

        }

        return dice;
    }
}
