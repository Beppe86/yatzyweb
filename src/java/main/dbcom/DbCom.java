package main.dbcom;

import java.sql.*;

//import main.soundfx.SoundEffects;

public class DbCom implements Runnable
{

    private static final String URL = "jdbc:sqlserver://62.101.44.83:1433;DatabaseName=KERPE";
    private static Connection myConn;
    private static String[] query;
    
    public DbCom(String[] query)
    {
        this.query = query;
    }
    
    @Override
    public void run()
    {
        insertIntoDb(query);
    }
    
 // Select with one parameter and print method
    public static void selectUsingOneParameter(String sql, String variable) {
 		
		if(connect()) {
	 		try {
				//String sql = "SELECT * FROM [Employees] WHERE [FirstName]=?";
		 		PreparedStatement statement = myConn.prepareStatement(sql);
				statement.setString(1, variable);
		 		ResultSet result = executeQuery(statement);
				printPlayerStats(result);
				
	 		}catch (SQLException e) {
	 			e.printStackTrace();
	 		}
	 	}
	}
	private static String printPlayerStats(ResultSet result) //under construction
	{
	    String out = "";
	
	    try
	    {
	        while (result.next())
	        {
	            out += ("GameID:       " + result.getString(1) + "      \tWinner:   " + result.getString(2) + "\n"
	            		+ "Winner score:   " + result.getString(3) + "Looser:   " + result.getString(4) + 
	            		"Looser score:   " + result.getString(5) + "Date ended:   ");
	        }
	        return out;
	    }
	    catch (Exception ex)
	    {
	        ex.printStackTrace();
	        return null;
	    }
	}
//------------------PlayerStats end--------------------


 //TopScores with print method
	public static String getTopScores(String sql) {
 		
 		if(connect()) {
 			
			try {
				PreparedStatement statement = myConn.prepareStatement(sql);
				ResultSet result = executeQuery(statement);
				return printTopScores(result);

			} catch (SQLException e) {
				e.printStackTrace();
			}
 		}
		return null;
 	}
 	
 	private static String printTopScores(ResultSet result)
    {
        String out = "";

        try
        {
            while (result.next())
            {
                out += ("\t" + result.getString(1) + "\t\t" + result.getString(2) + "\n");
            }
            return "--------------------Player name-------------------------Top score----------\n\n" + out;
        }		   			
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }
//-------------------TopScores end---------------------------------- 

    public static void insertIntoDb(String[] querys)
    {
        if (connect())
        {
            try
            {
                int rowsChanged = 0;
                int counter = 0;
                for (String query : querys)
                {
                    PreparedStatement statement = DbCom.myConn.prepareStatement(query);
                    rowsChanged += executeNonQuery(statement);
                    System.out.println("Uploaded" + ++counter + "\n");
                }
                System.out.println("Number of rows changed: " + rowsChanged);
                disconnect();
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }
        }
    }

    
    //SELECT
    private static ResultSet executeQuery(PreparedStatement sql)
    {
        try
        {
            return sql.executeQuery();

        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    //INSERT, DELETE or UPDATE
    private static int executeNonQuery(PreparedStatement sql)
    {
        try
        {
            return sql.executeUpdate();

        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return -1;
        }
    }
    
    
    
    
    
    public static boolean connect()
    {
        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            myConn = DriverManager.getConnection(URL, "KerpeAdmin", "kerpe");
            System.out.println("Connected...");
            return true;

        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
            //SoundEffects.CRASH.play();
            System.out.println("Connection failure!");
            return false;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            //SoundEffects.CRASH.play();
            System.out.println("Connection failure!");
            return false;
        }
    }

    public static void disconnect()
    {
        try
        {
            myConn.close();
            System.out.println("Disconnected...");
            //return true;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            System.out.println("Can't disconnect!");
            //return false;
        }
    }
}
